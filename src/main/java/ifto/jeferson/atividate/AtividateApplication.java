package ifto.jeferson.atividate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AtividateApplication {

	public static void main(String[] args) {
		SpringApplication.run(AtividateApplication.class, args);
	}

}
