package ifto.jeferson.atividate.entitys;
// import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String descricao;

    private Double valor;

    // @JsonIgnore
    @OneToMany(mappedBy = "produto")
    private List<ItemVenda> itemVendas = new ArrayList<ItemVenda>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public List<ItemVenda> getItemVendas() {
        return itemVendas;
    }

    public void setItemVendas(List<ItemVenda> itemVendas) {
        this.itemVendas = itemVendas;
    }    

    public void addItemVenda(ItemVenda itemVenda) {
        this.itemVendas.add(itemVenda);
    }    

    @Override
    public String toString() {
        return 
        "{ \"id\" : " + this.id + ", \"descricao\" : \"" + this.descricao + "\", \"valor\" : " + this.valor +"}";
    }

}