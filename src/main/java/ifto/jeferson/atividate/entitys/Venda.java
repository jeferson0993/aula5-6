package ifto.jeferson.atividate.entitys;

import javax.persistence.*;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Scope("session")
@Component
@Entity
public class Venda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "venda", cascade = CascadeType.PERSIST)
    private List<ItemVenda> itemVendas = new ArrayList<ItemVenda>();

    private LocalDate data = LocalDate.now();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getData() {
        return data;
    }

    public double total() {
        double total = 0.0;
        for(ItemVenda i: itemVendas){
            total += i.total();
        }
        return total;
    }

    public List<ItemVenda> getItemVendas() {
        return this.itemVendas;
    }

    public void setItemVendas(List<ItemVenda> itemVendas) {
        this.itemVendas = itemVendas;
    }

    public void addItemVenda(ItemVenda itemVenda) {
        this.itemVendas.add(itemVenda);
    }

    public void removeItemVenda(ItemVenda itemVenda) {
        this.itemVendas.remove(itemVenda);
    }

    @Override
    public String toString() {
        return 
        "{ "+
            "\"id\" : " + this.id +
            ", \"data\" : " + this.data +
            ", \"total\" : " + this.total()+
            ", \"items\" : " + this.itemVendas +
        " }";
    }

}