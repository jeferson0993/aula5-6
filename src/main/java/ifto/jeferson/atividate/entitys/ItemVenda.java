package ifto.jeferson.atividate.entitys;

import javax.persistence.*;

@Entity
public class ItemVenda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    private int qtd;

    @OneToOne
    @JoinColumn(name = "produto_id")
    private Produto produto;

    /* @JoinColumn(name = "venda_id") */
    @ManyToOne
    private Venda venda;

    public Long getId() {
        return id;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    public double total() {
        return qtd * produto.getValor();
    }

    @Override
    public String toString() {
        return 
        "{ \"id\":" + this.id + ", \"qtd\" :" + this.qtd + ", \"produto\" :" + this.produto + "}";
    }

}