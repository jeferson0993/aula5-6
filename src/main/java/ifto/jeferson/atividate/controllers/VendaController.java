package ifto.jeferson.atividate.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ifto.jeferson.atividate.entitys.ItemVenda;
import ifto.jeferson.atividate.entitys.Venda;

import ifto.jeferson.atividate.repositorys.ProdutoRepository;
import ifto.jeferson.atividate.repositorys.VendaRepository;

@Controller
@Transactional
@Scope("request")
@RequestMapping("vendas")
public class VendaController {
    
    @Autowired
    Venda venda;
    
    @Autowired
    VendaRepository vendaRepository;
    
    @Autowired
    ProdutoRepository produtoRepository;
        
    public VendaController() {}

    @GetMapping("/list")
    public ModelAndView listar(ModelMap model) {        
        model.addAttribute("vendas", vendaRepository.vendas());
        return new ModelAndView("/vendas/list", model);
    }

    @GetMapping("/form")
    public ModelAndView form(ModelMap model, ItemVenda itemVenda) {
        model.addAttribute("produtos", produtoRepository.produtos());
        return new ModelAndView("/vendas/form", model);
    }
    
    @PostMapping("/save")
    public ModelAndView save(Venda venda) {
        vendaRepository.save(venda);
        this.venda.getItemVendas().clear();
        return new ModelAndView("redirect:/vendas/list");
    }

    @GetMapping("/remove/{id}")
    public ModelAndView remove(@PathVariable("id") Long id) {
        vendaRepository.remove(id);
        return new ModelAndView("redirect:/vendas/list");
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id, ModelMap model) {
        model.addAttribute("venda", vendaRepository.venda(id));
        model.addAttribute("produtos", produtoRepository.produtos());
        return new ModelAndView("/vendas/form", model);
    }

    @PostMapping("/update")
    public ModelAndView update(Venda venda) {
        vendaRepository.update(venda);
        return new ModelAndView("redirect:/vendas/list");
    }

    @PostMapping("/addItem")
    public ModelAndView addItemVenda(
            ItemVenda itemVenda
    ) {
        ModelAndView model = new ModelAndView("redirect:/vendas/form");
        itemVenda.setProduto(
            produtoRepository.produto(itemVenda.getProduto().getId())
        );
        venda.addItemVenda(itemVenda);
        return model;
    }
}
