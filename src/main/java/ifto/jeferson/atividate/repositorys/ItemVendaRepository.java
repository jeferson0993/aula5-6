package ifto.jeferson.atividate.repositorys;

import java.util.List;

import javax.persistence.*;

import org.springframework.stereotype.Repository;

import ifto.jeferson.atividate.entitys.ItemVenda;


@Repository
public class ItemVendaRepository {
    @PersistenceContext
    private EntityManager em;
    public List<ItemVenda> itemVendas(){
        Query query = em.createQuery("from ItemVenda");
        return query.getResultList();
    }
    public void save(ItemVenda itemVenda){
        em.persist(itemVenda);
    }
    public ItemVenda itemVenda(Long id){
        return em.find(ItemVenda.class, id);
    }
    public void remove(Long id){
        ItemVenda p = em.find(ItemVenda.class, id);
        em.remove(p);
    }
    public void update(ItemVenda itemVenda){
        em.merge(itemVenda);
    }
}