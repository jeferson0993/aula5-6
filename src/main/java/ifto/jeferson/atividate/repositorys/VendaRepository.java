package ifto.jeferson.atividate.repositorys;

import java.util.*;

import javax.persistence.*;

import org.springframework.stereotype.Repository;

import ifto.jeferson.atividate.entitys.*;

@Repository
public class VendaRepository {
    @PersistenceContext
    private EntityManager em;
    public List<Venda> vendas(){
        Query query = em.createQuery("from Venda");
        return query.getResultList();
    }
    public void save(Venda venda){
        em.persist(venda);
    }
    public Venda venda(Long id){
        return em.find(Venda.class, id);
    }
    public void remove(Long id){
        Venda p = em.find(Venda.class, id);
        em.remove(p);
    }
    public void update(Venda venda){
        em.merge(venda);
    }
}