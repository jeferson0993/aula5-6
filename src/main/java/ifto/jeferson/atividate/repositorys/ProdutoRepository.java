package ifto.jeferson.atividate.repositorys;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import ifto.jeferson.atividate.entitys.Produto;

@Repository
public class ProdutoRepository {
    @PersistenceContext
    private EntityManager em;

    public ProdutoRepository() {}

    public List<Produto> produtos(){
        Query query = em.createQuery("from Produto");
        return query.getResultList();
    }
    public void save(Produto produto){
        em.persist(produto);
    }
    public Produto produto(Long id){
        return em.find(Produto.class, id);
    }
    public void remove(Long id){
        Produto p = em.find(Produto.class, id);
        em.remove(p);
    }
    public void update(Produto produto){
        em.merge(produto);
    }
}
